import 'dart:io';

import 'package:cr/cr.dart' as cr;
import 'package:cr/tools/appinfo_update.dart';
import 'package:test/test.dart';

void main() {
  AppInfoUpdate("pubspec.yaml").writeAppInfoDartFile("lib/model/appinfo.dart");

  // test('calculate', () {
  //   expect(calculate(), 49);
  // });

  // todo: need to add actual logic that adds expect calls for what is actually
  // expected to be in the local data model after the command has completed!!!
  // this will ensure that tests fail correctly when code changes!!!

  String verbosity = '4';

  test('payload help', () async {
    AppInfoUpdate("pubspec.yaml").verifyLatestVersionFromPubSpec();
    int retVal = await cr.run(['payload', '--help']);
    expect(retVal, 0);
  });
  test('scanfs1', () async {
    int retVal =
        await cr.run(['scanfs', '-v', verbosity, '--inclSubStr', 'cr-cli/lib']);
    expect(retVal, 0);
  });
  test('scanfs2', () async {
    int retVal = await cr.run(['scanfs', 'test/src', 'test/.coderockit']);
    expect(retVal, 0);
  });
  test('scanfs3', () async {
    int retVal = await cr.run(['scanfs', 'test/src']);
    expect(retVal, 0);
  });
  test('scanfs4', () async {
    int retVal =
        await cr.run(['scanfs', '-b', 'test/src', '-c', 'test/.coderockit']);
    expect(retVal, 0);
  });
  test('scanfs5', () async {
    int retVal = await cr.run([
      'scanfs',
      'yada1',
      'yada2',
      '-b',
      'test/src',
      '-c',
      'test/.coderockit'
    ]);
    expect(retVal, 0);
  });
  test('scanfs6', () async {
    int retVal =
        await cr.run(['scanfs', 'yada1', 'test/.coderockit', '-b', 'test/src']);
    expect(retVal, 0);
  });
  test('scanfs7', () async {
    int retVal =
        await cr.run(['scanfs', '--verbose', verbosity, '-f', 'test/src']);
    expect(retVal, 0);
  });
  test('scanfs8', () async {
    int retVal = await cr.run(['scanfs', '--verbose', verbosity, 'werwer']);
    expect(retVal, 1);
  });
  test('scanfs9', () async {
    int retVal = await cr.run([
      'scanfs',
      '--verbose',
      verbosity,
      'test/src',
      'test/.coderockit',
      'yada4',
      'yada5'
    ]);
    expect(retVal, 0);
  });
  test('scanfs10', () async {
    int retVal = await cr.run([
      'scanfs',
      'test/src',
      'test/.coderockit',
      '-v',
      verbosity,
      '-l',
      '-e',
      'test4'
    ]);
    expect(retVal, 0);
  });

  // test('payload help', () async {
  //   int retVal = await cr.run(['payload', '--help']);
  //   expect(retVal, 0);
  // });
  // test('payload1', () async {
  //   int retVal = await cr.run(['payload', '-v', verbosity]);
  //   expect(retVal, 0);
  // });
  // // keep payload1.1 test because it tests all of the code that finds existing payloads
  // test('payload1.1', () async {
  //   int retVal = await cr.run(['payload', '-v', verbosity]);
  //   expect(retVal, 0);
  // });
  // test('payload3', () async {
  //   File('test/src/xml/test5.xml').copySync('test/src/xml/orig.test5.xml');
  //   File('test/src/xml/test5.1.xml').copySync('test/src/xml/test5.xml');
  //   int retVal = await cr.run(['payload', '-v', verbosity]);
  //   File('test/src/xml/test5.2.xml').copySync('test/src/xml/test5.xml');
  //   retVal = await cr.run(['payload', '-v', verbosity, '--no-replace']);
  //   File('test/src/xml/orig.test5.xml').renameSync('test/src/xml/test5.xml');
  //   expect(retVal, 0);
  // });
  // test('payload2', () async {
  //   int retVal = await cr.run(['payload', '-v', verbosity, '-l']);
  //   expect(retVal, 0);
  // });
  // test('scanfs11', () async {
  //   await File('test/src/xml/test5.xml').rename('test/src/xml/orig.test5.xml');
  //   int retVal = await cr.run(['scanfs', 'test/src', '-v', verbosity, '-u']);
  //   expect(retVal, 0);
  //   await File('test/src/xml/orig.test5.xml').rename('test/src/xml/test5.xml');
  // });
  // test('payload2.1', () async {
  //   int retVal = await cr.run(['payload', '-v', verbosity, '-l']);
  //   expect(retVal, 0);
  // });

  // // NOTE: version command is done and working
  // test('version', () async {
  //   int retVal = await cr.run(['version']);
  //   expect(retVal, 0);
  // });

  // test('push', () async {
  //   int retVal = await cr.run(['push']);
  //   expect(retVal, 0);
  // });

  // test('pull', () async {
  //   int retVal = await cr.run(['pull']);
  //   expect(retVal, 0);
  // });

  // test('import', () async {
  //   int retVal = await cr.run(['import']);
  //   expect(retVal, 0);
  // });

  // test('status', () async {
  //   int retVal = await cr.run(['status']);
  //   expect(retVal, 0);
  // });

  // test('login', () async {
  //   int retVal = await cr.run(['login']);
  //   expect(retVal, 0);
  // });

  // test('plugin', () async {
  //   int retVal = await cr.run(['plugin']);
  //   expect(retVal, 0);
  // });

  // test('diff', () async {
  //   int retVal = await cr.run(['diff']);
  //   expect(retVal, 0);
  // });
}
