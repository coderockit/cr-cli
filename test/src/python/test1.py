# This program adds two numbers
# -+=coderockit:/@python-payload1/sum@^3.0.21/print-sum]==
num1 = 1.5
num2 = 6.3

# Add two numbers
sum = num1 + num2

# Display the sum
print('The sum of {0} and {1} is {2}'.format(num1, num2, sum))
# -+=coderockit:/@]==