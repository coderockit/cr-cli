#include <iostream>
#include <fstream>
using namespace std;

int main() {
  /* -+=coderockit:/@cpp-payload1/file@^0.0.8/writefile]== */
  // Create and open a text file
  ofstream MyFile("filename.txt");

  // Write to the file
  MyFile << "Files can be tricky, but it is fun enough!";

  // Close the file
  MyFile.close();
  /* -+=coderockit:/@]== */
}