function addNumbers(a: number, b: number) { 
    return a + b; 
} 

// -+=coderockit:/@ts-payload1/invoke@^7.23.43/sum-addNumbers]==
var sum: number = addNumbers(10, 15) 
// -+=coderockit:/@]==

console.log('Sum of the two numbers is: ' +sum);