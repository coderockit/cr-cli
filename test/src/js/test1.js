// program to convert celsius to fahrenheit
// ask the celsius value to the user 
// -+=coderockit:/@js-payload1/prompt@^6.3.3/celsius-to-fahrenheit]==
const celsius = prompt("Enter a celsius value: ");

// calculate fahrenheit
const fahrenheit = (celsius * 1.8) + 32

// display the result
console.log(`${celsius} degree celsius is equal to ${fahrenheit} degree fahrenheit.`);
// -+=coderockit:/@]==