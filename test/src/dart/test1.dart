void main() {
  var result = fibonacci(20);
  print('Hello, World: $result');
}

// -+=coderockit:/@dart-payload1/func@^21.8.90/fibonacci]==
int fibonacci(int n) {
  if (n == 0 || n == 1) return n;
  return fibonacci(n - 1) + fibonacci(n - 2);
}
// -+=coderockit:/@]==
