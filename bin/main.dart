import 'dart:io';
import 'dart:isolate';
import 'package:cr/cr.dart' as cr;
import 'package:cr/tools/appinfo_update.dart';

Future<void> main(List<String> arguments) async {
  AppInfoUpdate("pubspec.yaml").verifyLatestVersionFromPubSpec();
  // print('Hello world!');
  int retVal = await cr.run(arguments);
  //print("main :: cr.run return code: $retVal");

  // NOTE: It seems that without these two lines then invoking
  // the cr command from the OS specific command line never returns
  // control back to the command prompt!!!
  // This is weird and is probably a bug that needs to be fixed...
  // It seems that the bug is related to the realm_dart database package we are using
  // If the realm_dart database never gets initialized then the command line
  // invocation exits cleanly but if the realm_dart database is initialized then
  // the invocation just hangs and never returns control to the command prompt
  Isolate.current.pause();
  exit(retVal);
}
