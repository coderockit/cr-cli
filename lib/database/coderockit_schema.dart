import 'package:realm_dart/realm.dart';

part 'coderockit_schema.g.dart';

@RealmModel()
class _FileWithPayloads {
  @PrimaryKey()
  late final int digest;
  // NOTE: filepath MUST NOT be modified once it is set!!!! This is because
  // the PrimaryKey digest is a hash of the filepath
  late final String filepath;

  @override
  String toString() {
    return filepath;
  }

  @override
  bool operator ==(Object other) =>
      other is _FileWithPayloads &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}

@RealmModel()
class _ContentVersion {
  @PrimaryKey()
  late final int digest;
  // NOTE: snippetContentDigest MUST NOT be modified once it is set!!!! This is because
  // the PrimaryKey digest is a hash of _SnippetContent.contentVersion index (aka contentVersionIndex) and snippetContentDigest
  late final int contentVersionIndex;
  late final int snippetContentDigest;

  late String semverPattern;
  late String? contentPath;
  late String? contentHash;
  // late String? contentSemverVersion;

  @override
  String toString() {
    return '\n      contentVersion ID [$digest] -- $semverPattern -- content index [$contentVersionIndex] -- content hash [$contentHash] -- content path [$contentPath]';
  }

  @override
  bool operator ==(Object other) =>
      other is _ContentVersion &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}

@RealmModel()
class _SnippetContent {
  @PrimaryKey()
  late final int digest;
  // NOTE: snippetDigest, fileDigest MUST NOT be modified once they are set!!!! This is because
  // the PrimaryKey digest is a hash of the namespace, payloadName, containerName, filepath, snippetID
  late final int snippetDigest;
  late final _FileWithPayloads? fileWithPayloads;

  late List<_ContentVersion> contentVersionList;

  String asString({bool includeContentVersionList = false}) {
    return '\n    snippetContent ID [$digest] -- file path [$fileWithPayloads]${includeContentVersionList ? ' -- $contentVersionList' : ''}';
  }

  @override
  String toString() {
    return asString(includeContentVersionList: true);
  }

  @override
  bool operator ==(Object other) =>
      other is _SnippetContent &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}

@RealmModel()
class _Snippet {
  @PrimaryKey()
  late final int digest;
  // NOTE: containerName, payloadDigest, snippetID MUST NOT be modified once they are set!!!! This is because
  // the PrimaryKey digest is a hash of the namespace, payloadName, containerName, snippetID
  late final int payloadDigest;
  late final String containerName;
  late final int snippetID;

  late List<_SnippetContent> contentList;

  String asString({bool includeContentList = false}) {
    return '\n  snippet ID [$digest] -- @[version]/$containerName-$snippetID${includeContentList ? ' -- $contentList' : ''}';
  }

  @override
  String toString() {
    return asString(includeContentList: true);
  }

  @override
  bool operator ==(Object other) =>
      other is _Snippet &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}

@RealmModel()
class _Payload {
  @PrimaryKey()
  late final int digest;
  // NOTE: namespace, payloadName MUST NOT be modified once it is set!!!! This is because
  // the PrimaryKey digest is a hash of the namespace, payloadName
  late final String namespace;
  late final String payloadName;

  late List<_Snippet> snippets;
  // late String? payloadHash; // payloadHash is a hash of all of the contentHash
  // late String? payloadVersion;

  String getFullName() {
    return '$namespace/$payloadName';
  }

  String asString({bool includeSnippets = false}) {
    return '\npayload ID [$digest] -- coderockit:/${getFullName()}${includeSnippets ? ' -- $snippets' : ''}';
  }

  @override
  String toString() {
    return asString(includeSnippets: true);
  }

  @override
  bool operator ==(Object other) =>
      other is _Payload &&
      other.runtimeType == runtimeType &&
      other.digest == digest;

  @override
  int get hashCode => digest.hashCode;
}
