// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coderockit_schema.dart';

// **************************************************************************
// RealmObjectGenerator
// **************************************************************************

class FileWithPayloads extends _FileWithPayloads with RealmEntity, RealmObject {
  FileWithPayloads(
    int digest,
    String filepath,
  ) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'filepath', filepath);
  }

  FileWithPayloads._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  String get filepath => RealmObject.get<String>(this, 'filepath') as String;
  @override
  set filepath(String value) => throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<FileWithPayloads>> get changes =>
      RealmObject.getChanges<FileWithPayloads>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(FileWithPayloads._);
    return const SchemaObject(FileWithPayloads, 'FileWithPayloads', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('filepath', RealmPropertyType.string),
    ]);
  }
}

class ContentVersion extends _ContentVersion with RealmEntity, RealmObject {
  ContentVersion(
    int digest,
    int contentVersionIndex,
    int snippetContentDigest,
    String semverPattern, {
    String? contentPath,
    String? contentHash,
  }) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'contentVersionIndex', contentVersionIndex);
    RealmObject.set(this, 'snippetContentDigest', snippetContentDigest);
    RealmObject.set(this, 'semverPattern', semverPattern);
    RealmObject.set(this, 'contentPath', contentPath);
    RealmObject.set(this, 'contentHash', contentHash);
  }

  ContentVersion._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  int get contentVersionIndex =>
      RealmObject.get<int>(this, 'contentVersionIndex') as int;
  @override
  set contentVersionIndex(int value) => throw RealmUnsupportedSetError();

  @override
  int get snippetContentDigest =>
      RealmObject.get<int>(this, 'snippetContentDigest') as int;
  @override
  set snippetContentDigest(int value) => throw RealmUnsupportedSetError();

  @override
  String get semverPattern =>
      RealmObject.get<String>(this, 'semverPattern') as String;
  @override
  set semverPattern(String value) =>
      RealmObject.set(this, 'semverPattern', value);

  @override
  String? get contentPath =>
      RealmObject.get<String>(this, 'contentPath') as String?;
  @override
  set contentPath(String? value) => RealmObject.set(this, 'contentPath', value);

  @override
  String? get contentHash =>
      RealmObject.get<String>(this, 'contentHash') as String?;
  @override
  set contentHash(String? value) => RealmObject.set(this, 'contentHash', value);

  @override
  Stream<RealmObjectChanges<ContentVersion>> get changes =>
      RealmObject.getChanges<ContentVersion>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(ContentVersion._);
    return const SchemaObject(ContentVersion, 'ContentVersion', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('contentVersionIndex', RealmPropertyType.int),
      SchemaProperty('snippetContentDigest', RealmPropertyType.int),
      SchemaProperty('semverPattern', RealmPropertyType.string),
      SchemaProperty('contentPath', RealmPropertyType.string, optional: true),
      SchemaProperty('contentHash', RealmPropertyType.string, optional: true),
    ]);
  }
}

class SnippetContent extends _SnippetContent with RealmEntity, RealmObject {
  SnippetContent(
    int digest,
    int snippetDigest, {
    FileWithPayloads? fileWithPayloads,
    Iterable<ContentVersion> contentVersionList = const [],
  }) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'snippetDigest', snippetDigest);
    RealmObject.set(this, 'fileWithPayloads', fileWithPayloads);
    RealmObject.set<RealmList<ContentVersion>>(this, 'contentVersionList',
        RealmList<ContentVersion>(contentVersionList));
  }

  SnippetContent._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  int get snippetDigest => RealmObject.get<int>(this, 'snippetDigest') as int;
  @override
  set snippetDigest(int value) => throw RealmUnsupportedSetError();

  @override
  FileWithPayloads? get fileWithPayloads =>
      RealmObject.get<FileWithPayloads>(this, 'fileWithPayloads')
          as FileWithPayloads?;
  @override
  set fileWithPayloads(covariant FileWithPayloads? value) =>
      throw RealmUnsupportedSetError();

  @override
  RealmList<ContentVersion> get contentVersionList =>
      RealmObject.get<ContentVersion>(this, 'contentVersionList')
          as RealmList<ContentVersion>;
  @override
  set contentVersionList(covariant RealmList<ContentVersion> value) =>
      throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<SnippetContent>> get changes =>
      RealmObject.getChanges<SnippetContent>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(SnippetContent._);
    return const SchemaObject(SnippetContent, 'SnippetContent', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('snippetDigest', RealmPropertyType.int),
      SchemaProperty('fileWithPayloads', RealmPropertyType.object,
          optional: true, linkTarget: 'FileWithPayloads'),
      SchemaProperty('contentVersionList', RealmPropertyType.object,
          linkTarget: 'ContentVersion',
          collectionType: RealmCollectionType.list),
    ]);
  }
}

class Snippet extends _Snippet with RealmEntity, RealmObject {
  Snippet(
    int digest,
    int payloadDigest,
    String containerName,
    int snippetID, {
    Iterable<SnippetContent> contentList = const [],
  }) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'payloadDigest', payloadDigest);
    RealmObject.set(this, 'containerName', containerName);
    RealmObject.set(this, 'snippetID', snippetID);
    RealmObject.set<RealmList<SnippetContent>>(
        this, 'contentList', RealmList<SnippetContent>(contentList));
  }

  Snippet._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  int get payloadDigest => RealmObject.get<int>(this, 'payloadDigest') as int;
  @override
  set payloadDigest(int value) => throw RealmUnsupportedSetError();

  @override
  String get containerName =>
      RealmObject.get<String>(this, 'containerName') as String;
  @override
  set containerName(String value) => throw RealmUnsupportedSetError();

  @override
  int get snippetID => RealmObject.get<int>(this, 'snippetID') as int;
  @override
  set snippetID(int value) => throw RealmUnsupportedSetError();

  @override
  RealmList<SnippetContent> get contentList =>
      RealmObject.get<SnippetContent>(this, 'contentList')
          as RealmList<SnippetContent>;
  @override
  set contentList(covariant RealmList<SnippetContent> value) =>
      throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<Snippet>> get changes =>
      RealmObject.getChanges<Snippet>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Snippet._);
    return const SchemaObject(Snippet, 'Snippet', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('payloadDigest', RealmPropertyType.int),
      SchemaProperty('containerName', RealmPropertyType.string),
      SchemaProperty('snippetID', RealmPropertyType.int),
      SchemaProperty('contentList', RealmPropertyType.object,
          linkTarget: 'SnippetContent',
          collectionType: RealmCollectionType.list),
    ]);
  }
}

class Payload extends _Payload with RealmEntity, RealmObject {
  Payload(
    int digest,
    String namespace,
    String payloadName, {
    Iterable<Snippet> snippets = const [],
  }) {
    RealmObject.set(this, 'digest', digest);
    RealmObject.set(this, 'namespace', namespace);
    RealmObject.set(this, 'payloadName', payloadName);
    RealmObject.set<RealmList<Snippet>>(
        this, 'snippets', RealmList<Snippet>(snippets));
  }

  Payload._();

  @override
  int get digest => RealmObject.get<int>(this, 'digest') as int;
  @override
  set digest(int value) => throw RealmUnsupportedSetError();

  @override
  String get namespace => RealmObject.get<String>(this, 'namespace') as String;
  @override
  set namespace(String value) => throw RealmUnsupportedSetError();

  @override
  String get payloadName =>
      RealmObject.get<String>(this, 'payloadName') as String;
  @override
  set payloadName(String value) => throw RealmUnsupportedSetError();

  @override
  RealmList<Snippet> get snippets =>
      RealmObject.get<Snippet>(this, 'snippets') as RealmList<Snippet>;
  @override
  set snippets(covariant RealmList<Snippet> value) =>
      throw RealmUnsupportedSetError();

  @override
  Stream<RealmObjectChanges<Payload>> get changes =>
      RealmObject.getChanges<Payload>(this);

  static SchemaObject get schema => _schema ??= _initSchema();
  static SchemaObject? _schema;
  static SchemaObject _initSchema() {
    RealmObject.registerFactory(Payload._);
    return const SchemaObject(Payload, 'Payload', [
      SchemaProperty('digest', RealmPropertyType.int, primaryKey: true),
      SchemaProperty('namespace', RealmPropertyType.string),
      SchemaProperty('payloadName', RealmPropertyType.string),
      SchemaProperty('snippets', RealmPropertyType.object,
          linkTarget: 'Snippet', collectionType: RealmCollectionType.list),
    ]);
  }
}
