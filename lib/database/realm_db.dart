import 'package:cr/database/coderockit_schema.dart';
import 'package:realm_dart/realm.dart';
import 'package:path/path.dart' as pathlib;

class RealmDB {
  static const String realmDbName = "coderockit.realm";

  String crDirPath;
  late LocalConfiguration dbConfig;
  late Realm realm;

  RealmDB(this.crDirPath) {
    dbConfig = Configuration.local([
      FileWithPayloads.schema,
      Payload.schema,
      Snippet.schema,
      SnippetContent.schema,
      ContentVersion.schema
    ], path: pathlib.join(pathlib.canonicalize(crDirPath), realmDbName));
    realm = Realm(dbConfig);
  }

  // void createOrUpdateFilesWithPayloads(
  //     List<FileWithPayloads> filesWithPayloads) {}

  void close() {
    realm.close();
  }
}
