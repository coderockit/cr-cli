import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:args/command_runner.dart';
import 'package:cr/commands/command_response.dart';

import 'package:cr/database/realm_db.dart';
import 'package:cr/native/coderockit_core.dart';
import 'package:cr/tools/logger.dart';
import 'package:logging/logging.dart';
import 'package:path/path.dart' as pathlib;
import 'package:crypto/crypto.dart';
import 'package:meta/meta.dart';

abstract class BaseCommand extends Command {
  static final Map<String, String> crEnv = Platform.environment;

  static final baseDirDefault = '.';
  static final crDirDefault = Platform.isWindows
      ? '${crEnv['HOMEDRIVE']}:\\${crEnv['HOMEPATH']}\\.coderockit'
      : '${crEnv['HOME']}/.coderockit';
  static final localContentFolder = 'localcontent';
  static final remoteContentFolder = 'remotecontent';

  static final excludeSubStrDefault = '.git,node_modules';
  static final includeSubStrDefault = '';
  static final excludeRegExpDefault = '';
  static final includeRegExpDefault = '';
  static final LineSplitter lineSplitter = LineSplitter();

  String? _baseDirPath;
  Directory? _baseDir;
  String? _crDirPath;
  Directory? _crDir;
  RealmDB? _realmDB;
  LogLevel? _logLevel;

  Future<int> validateBaseDirANDCrDir() async {
    if (argResults?.rest != null && argResults!.rest.isNotEmpty) {
      // if no options are specified then the next 2 args after the command are taken to be in order 1) baseDir and 2) crDir
      // which means that if only a crDir needs to be specified then you MUST use the crDir option switch!!!
      if (globalResults!.options.contains('baseDir')) {
        log.warning('WARNING :: Not using argument: ${argResults!.rest[0]}');
      } else {
        baseDirPath = argResults!.rest[0];
      }

      if (argResults!.rest.length > 1) {
        if (globalResults!.options.contains('crDir')) {
          log.warning('WARNING :: Not using argument: ${argResults!.rest[1]}');
        } else {
          crDirPath = argResults!.rest[1];
        }
      }

      if (argResults!.rest.length > 2) {
        log.warning(
            'WARNING :: Not using extra arguments: ${argResults!.rest.sublist(2)}');
      }
    }
    baseDirPath = pathlib.canonicalize(baseDirPath);
    crDirPath = pathlib.canonicalize(crDirPath);

    _baseDir = Directory(baseDirPath);
    _crDir = Directory(crDirPath);
    bool baseDirExists = await _baseDir!.exists();
    bool crDirExists = await _crDir!.exists();
    int returnCode = 0;

    if (!baseDirExists || !crDirExists) {
      if (!baseDirExists) {
        print('[1] ERROR: The baseDir folder "$baseDirPath" does not exist!!!');
        returnCode = 1;
      }
      if (!crDirExists) {
        print('[2] ERROR: The crDir folder "$crDirPath" does not exist!!!');
        returnCode = 1;
      }
    }
    return returnCode;
  }

  LogLevel get verbose {
    return _logLevel!;
  }

  Directory? get baseDir {
    return _baseDir;
  }

  Directory? get crDir {
    return _crDir;
  }

  String get baseDirPath {
    _baseDirPath ??=
        pathlib.canonicalize(globalResults?['baseDir'] ?? baseDirDefault);
    return _baseDirPath!;
  }

  set baseDirPath(String newBaseDirPath) {
    _baseDirPath = newBaseDirPath;
  }

  String get crDirPath {
    _crDirPath ??=
        pathlib.canonicalize(globalResults?['crDir'] ?? crDirDefault);
    return _crDirPath!;
  }

  set crDirPath(String newCrDirPath) {
    _crDirPath = newCrDirPath;
  }

  bool initDB() {
    // print("Running command with baseDir: $baseDirPath");
    // print("Using config, metadata, and index database in crDir: $crDirPath");
    _realmDB ??= RealmDB(crDirPath);
    return true;
  }

  RealmDB? get db {
    return _realmDB;
  }

  bool includeFullPath(
      String fullpath,
      List<String> exclSubStrs,
      List<String> inclSubStrs,
      List<String> exclRegExps,
      List<String> inclRegExps) {
    if ((exclSubStrs.isEmpty && exclRegExps.isEmpty) ||
        (exclSubStrs.isNotEmpty &&
            exclRegExps.isEmpty &&
            !containsOneOfSubStrs(fullpath, exclSubStrs)) ||
        (exclSubStrs.isEmpty &&
            exclRegExps.isNotEmpty &&
            !mathchesOneOfRegExps(fullpath, exclRegExps)) ||
        (exclSubStrs.isNotEmpty &&
            exclRegExps.isNotEmpty &&
            !containsOneOfSubStrs(fullpath, exclSubStrs) &&
            !mathchesOneOfRegExps(fullpath, exclRegExps))) {
      if ((inclSubStrs.isEmpty && inclRegExps.isEmpty) ||
          (inclSubStrs.isNotEmpty &&
              containsOneOfSubStrs(fullpath, inclSubStrs)) ||
          (inclRegExps.isNotEmpty &&
              mathchesOneOfRegExps(fullpath, inclRegExps))) {
        // if(verbose) {
        //   print("Including full path: $fullpath");
        // }
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  // Future<List<File>> allFilesAndLinksRecursively(
  //     Directory dir,
  //     List<String> exclSubStrs,
  //     List<String> inclSubStrs,
  //     List<String> exclRegExps,
  //     List<String> inclRegExps) async {
  //   // NOTE: Skip all files and directories containing the hidden .coderockit folder followed by the RealmDB.realmDbName
  //   exclSubStrs.add('.coderockit/${RealmDB.realmDbName}');

  //   var files = <File>[];
  //   var completer = Completer<List<File>>();
  //   var lister = dir.list(recursive: true, followLinks: true);

  //   lister.listen((file) {
  //     String fullpath = pathlib.canonicalize(file.absolute.path);
  //     if (includeFullPath(
  //         fullpath, exclSubStrs, inclSubStrs, exclRegExps, inclRegExps)) {
  //       // if(verbose) {
  //       //   print("Including full path: $fullpath");
  //       // }
  //       if (file is File) {
  //         files.add(file);
  //       } else if (file is Link) {
  //         if (verbose) {
  //           print("Could NOT resolve link $file so skipping it!!!");
  //         }
  //       }
  //     }
  //   },
  //       onDone: () => completer.complete(files),
  //       onError: (err) => print('[3] Error: $err'));

  //   return completer.future;
  // }

  bool containsOneOfSubStrs(String str, List<String> substrs) {
    for (String substr in substrs) {
      if (str.contains(substr)) {
        return true;
      }
    }
    if (substrs.isEmpty) {
      return true;
    }
    return false;
  }

  bool mathchesOneOfRegExps(String str, List<String> regexps) {
    for (String regexp in regexps) {
      RegExp re = RegExp(
        regexp,
        caseSensitive: false,
        multiLine: false,
      );
      if (re.hasMatch(str)) {
        return true;
      }
    }
    if (regexps.isEmpty) {
      return true;
    }
    return false;
  }

  Future<String> getHashOfFilesContent(File contentFile) async {
    return getHashOfContent(await contentFile.readAsString());
  }

  String getHashOfContent(String content) {
    return base64Url.encode(sha1.convert(utf8.encode(content)).bytes);
  }

  // Future<List<Payload>> getPayloadsFromFile(File toSearch) async {
  //   print('Searching file $toSearch for payload tags');
  //   return <Payload>[];
  // }

  @override
  @nonVirtual
  Future<CommandResponse> run() async {
    int returnCode = 0;

    if (_logLevel == null) {
      int? verboseVal = int.tryParse(globalResults?['verbose']);
      _logLevel = (verboseVal == null ||
              verboseVal < LogLevel.ERROR.index ||
              verboseVal > LogLevel.DEBUG.index)
          ? LogLevel.ERROR
          : LogLevel.values[verboseVal];
      if (_logLevel == LogLevel.ERROR) {
        Logger.root.level = Level.SEVERE;
      } else if (_logLevel == LogLevel.WARN) {
        Logger.root.level = Level.WARNING;
      } else if (_logLevel == LogLevel.INFO) {
        Logger.root.level = Level.INFO;
      } else if (_logLevel == LogLevel.DEBUG) {
        Logger.root.level = Level.ALL;
      } else {
        Logger.root.level = Level.SEVERE;
      }
    }

    log.finest('argResults name: ${argResults?.name}');
    log.finest('argResults arguments: ${argResults?.arguments}');
    log.finest('argResults command: ${argResults?.command}');
    log.finest('argResults rest: ${argResults?.rest}');
    log.finest('argResults options: ${argResults?.options}');
    log.finest('argResults baseDir: ${globalResults?['baseDir']}');
    log.finest('argResults crDir: ${globalResults?['crDir']}');
    log.finest('globalResults verbose: ${globalResults?['verbose']}');

    returnCode = await validateBaseDirANDCrDir();

    if (returnCode == 0) {
      return await runCmd();
    } else {
      return CommandResponse(returnCode, verbose, db);
    }
  }

  Future<CommandResponse> runCmd();
}
