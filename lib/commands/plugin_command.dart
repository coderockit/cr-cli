import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';

class PluginCommand extends BaseCommand {
  @override
  final name = "plugin";
  @override
  final description =
      "Manage the CodeRockIT plugins registered in this project.";

  PluginCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);
    return CommandResponse(0, verbose, db);
  }
}
