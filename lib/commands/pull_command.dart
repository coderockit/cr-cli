import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';

class PullCommand extends BaseCommand {
  @override
  final name = "pull";
  @override
  final description =
      "Pull all of the prepared payloads from the CodeRockIT API server.";

  PullCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);

    // setting the author data to this user

    return CommandResponse(0, verbose, db);
  }
}
