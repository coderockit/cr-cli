import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';
import 'package:cr/model/appinfo.dart';

class VersionCommand extends BaseCommand {
  @override
  final name = "version";
  @override
  final description = "Show the CodeRockIT cr command line tools version.";

  VersionCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);
    print("Name: ${appInfo['name']}");
    print("Description: ${appInfo['description']}");
    print("Version: ${appInfo['version']}");
    return CommandResponse(0, verbose, db);
  }
}
