import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';

class DiffCommand extends BaseCommand {
  @override
  final name = "diff";
  @override
  final description =
      "Show the diffs between the CodeRockIT payloads and their publish versions.";

  DiffCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);
    return CommandResponse(0, verbose, db);
  }
}
