import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:cr/commands/base_command.dart';
import 'package:cr/commands/command_response.dart';
import 'package:cr/database/coderockit_schema.dart';
import 'package:cr/model/payload_file.dart';

import 'package:cr/model/payload_files_list.dart';
import 'package:cr/model/payload_list.dart';
import 'package:cr/model/payload_tag.dart';
import 'package:cr/native/coderockit_core.dart';
import 'package:cr/tools/logger.dart';
import 'package:path/path.dart' as pathlib;
import 'package:realm_dart/realm.dart';
import 'package:xxh3/xxh3.dart';

class ScanfsCommand extends BaseCommand {
  @override
  final name = "scanfs";
  @override
  final description =
      "Scan filesystem for files containing CodeRockIT payloads in the specified directory OR list files in the existing already scanned index.";

  ScanfsCommand() {
    argParser.addOption('exclSubStr',
        abbr: 'e',
        help:
            'Comma separated list of substrings of file/directory/link paths to exclude. Excludes are applied first. Defaults to "${BaseCommand.excludeSubStrDefault}"');
    argParser.addOption('inclSubStr',
        abbr: 'i',
        help:
            'Comma separated list of substrings of file/directory/link paths to include. Excludes are applied first. Defaults to "${BaseCommand.includeSubStrDefault}"');
    argParser.addOption('exclRegExp',
        abbr: 'x',
        help:
            'Comma separated list of regular expressions of file/directory/link paths to exclude. Excludes are applied first. Defaults to "${BaseCommand.excludeRegExpDefault}"');
    argParser.addOption('inclRegExp',
        abbr: 'n',
        help:
            'Comma separated list of regular expressions of file/directory/link paths to include. Excludes are applied first. Defaults to "${BaseCommand.includeRegExpDefault}"');
    argParser.addOption('folders',
        abbr: 'f',
        help:
            'Comma separated list of explicit folders to scan which overrides the baseDir. Defaults to ""');
    argParser.addFlag('list',
        abbr: 'l',
        help: 'List already scanned files from database index.',
        defaultsTo: false);
    argParser.addFlag('cleanup',
        abbr: 'u',
        help:
            'For scanned files that no longer exist, remove ${BaseCommand.localContentFolder} files and data from database index.',
        defaultsTo: false);
  }

  Future<bool> hasPayloads(File toSearch) async {
    log.info('Checking $toSearch for payload tags');

    bool foundPayload = false;
    Stream<String> lines = toSearch
        .openRead()
        .transform(utf8.decoder) // Decode bytes to UTF-8.
        .transform(LineSplitter()); // Convert stream to individual lines.
    try {
      bool foundBeginning = false;
      await for (var line in lines) {
        // print('$line: ${line.length} characters');
        int beginIndex = line.indexOf(PayloadTag.beginTag);
        if (foundBeginning ||
            (beginIndex != -1 &&
                line.contains(PayloadTag.endOfBeginTag,
                    beginIndex + PayloadTag.beginTag.length))) {
          if (foundBeginning) {
            int endIndex = line.indexOf(PayloadTag.endTag);
            if (endIndex != -1) {
              foundPayload = true;
              throw AbortSearchingInFile();
            }
          } else {
            foundBeginning = true;
          }
        }
      }
      // print('File is now closed.');
    } catch (e) {
      if (e is! AbortSearchingInFile) {
        log.severe('[4] Error: $e');
      }
    }

    return foundPayload;
  }

  @override
  Future<CommandResponse> runCmd() async {
    int returnCode = 0;

    // print('argResults name: ${argResults?.name}');
    // print('argResults arguments: ${argResults?.arguments}');
    // print('argResults command: ${argResults?.command}');
    // print('argResults rest: ${argResults?.rest}');
    // print('argResults options: ${argResults?.options}');
    // print('argResults baseDir: ${argResults?['baseDir']}');
    // print('argResults crDir: ${argResults?['crDir']}');
    // print('globalResults verbose: ${globalResults?['verbose']}');

    List<String> exclSubStrs =
        (argResults?['exclSubStr'] ?? BaseCommand.excludeSubStrDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();
    List<String> inclSubStrs =
        (argResults?['inclSubStr'] ?? BaseCommand.includeSubStrDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();
    List<String> exclRegExps =
        (argResults?['exclRegExp'] ?? BaseCommand.excludeRegExpDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();
    List<String> inclRegExps =
        (argResults?['inclRegExp'] ?? BaseCommand.includeRegExpDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();

    // returnCode = await validateBaseDirANDCrDir();

    // IMPORTANT NOTE: Only invoke initDB AFTER crDirPath is set to it's final value
    initDB();

    if (argResults?['cleanup'] == true) {
      // cleanup files that have been deleted since they were last scanned
      PayloadFilesList listOfPayloadFiles = PayloadFilesList(db);
      // listOfPayloadFiles.printScannedFiles(includeFullPath, exclSubStrs,
      //     inclSubStrs, exclRegExps, inclRegExps);
      RealmResults<FileWithPayloads>? files =
          await listOfPayloadFiles.getFiles();
      print('Cleaned-up these files from database index:');
      List<FileWithPayloads> dataToRemove = <FileWithPayloads>[];
      if (files != null) {
        for (var fileWithPayloads in files) {
          if (includeFullPath(fileWithPayloads.filepath, exclSubStrs,
              inclSubStrs, exclRegExps, inclRegExps)) {
            File checkFile = File(fileWithPayloads.filepath);
            if (!await checkFile.exists()) {
              dataToRemove.add(fileWithPayloads);
              print(
                  '${fileWithPayloads.filepath} -- exists: ${await checkFile.exists()}');
            }
          }
        }
      }
      listOfPayloadFiles.cleanupFiles(dataToRemove);
    } else if (argResults?['list'] == true) {
      // int lastPayloadFileDigest = MIN_INT64;
      // List<PayloadFile> fileList = crCore.getScannedFilesList(
      //     baseDir!,
      //     crDir!,
      //     verbose,
      //     exclSubStrs,
      //     inclSubStrs,
      //     exclRegExps,
      //     inclRegExps,
      //     lastPayloadFileDigest);
      // for (var fileWithPayloads in fileList) {
      //   print(
      //       '${fileWithPayloads.filePath} -- exists: ${fileWithPayloads.exists}');
      // }
    } else {
      // now loop over files and dirs recursively looking for coderockit payloads
      if (argResults?['folders'] == null) {
        returnCode = crCore.scanForFilesWithPayloads(baseDir!, crDir!, verbose,
            exclSubStrs, inclSubStrs, exclRegExps, inclRegExps);
      } else {
        List<String> folders =
            argResults?['folders'].split(',').where((s) => !s.isEmpty).toList();
        returnCode = crCore.scanForFilesWithPayloads(baseDir!, crDir!, verbose,
            exclSubStrs, inclSubStrs, exclRegExps, inclRegExps,
            folders: folders);
      }
    }

    return CommandResponse(returnCode, verbose, db);
  }
}
