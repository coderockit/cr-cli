import 'package:cr/database/realm_db.dart';
import 'package:cr/native/coderockit_core.dart';

class CommandResponse {
  int exitCode;
  LogLevel verbose;
  RealmDB? db;

  CommandResponse(this.exitCode, this.verbose, this.db);
}
