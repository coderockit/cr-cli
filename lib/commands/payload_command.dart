import 'dart:convert';
import 'dart:io';
import 'package:cr/model/payload_tag.dart';
import 'package:cr/tools/logger.dart';
import 'package:path/path.dart' as pathlib;
import 'package:cr/commands/base_command.dart';
import 'package:cr/commands/command_response.dart';
import 'package:cr/database/coderockit_schema.dart';
import 'package:cr/model/parse_result.dart';
import 'package:cr/model/payload_files_list.dart';
import 'package:cr/model/payload_list.dart';
import 'package:realm_dart/realm.dart';

class PayloadCommand extends BaseCommand {
  @override
  final name = "payload";
  @override
  final description =
      "Prepare all of the CodeRockIT payloads to be published to the API server OR imported into your code.";

  PayloadCommand() {
    argParser.addOption('exclSubStr',
        abbr: 'e',
        help:
            'Comma separated list of substrings of file/directory/link paths to exclude. Excludes are applied first. Defaults to "${BaseCommand.excludeSubStrDefault}"');
    argParser.addOption('inclSubStr',
        abbr: 'i',
        help:
            'Comma separated list of substrings of file/directory/link paths to include. Excludes are applied first. Defaults to "${BaseCommand.includeSubStrDefault}"');
    argParser.addOption('exclRegExp',
        abbr: 'x',
        help:
            'Comma separated list of regular expressions of file/directory/link paths to exclude. Excludes are applied first. Defaults to "${BaseCommand.excludeRegExpDefault}"');
    argParser.addOption('inclRegExp',
        abbr: 'n',
        help:
            'Comma separated list of regular expressions of file/directory/link paths to include. Excludes are applied first. Defaults to "${BaseCommand.includeRegExpDefault}"');
    argParser.addFlag('list',
        abbr: 'l', help: 'List already scanned files.', defaultsTo: false);
    argParser.addFlag('replace',
        abbr: 'r',
        help:
            'Replace existing payload content files in crDir with parsed payload content files.',
        defaultsTo: true);
  }

  ParseResult parsePayloadFromBeginTag(List<Payload> payloads,
      FileWithPayloads fileWithPayloads, String beginCrTag) {
    // print('Parsing payload tag: $beginCrTag');
    Payload? payload;
    Snippet? snippet;
    SnippetContent? snippetContent;
    ContentVersion? contentVersion;

    PayloadTag payloadTag = PayloadTag(beginCrTag, fileWithPayloads.filepath);
    if (payloadTag.isParsed) {
      payloads.forEach((Payload p) {
        if (p.digest == payloadTag.payloadDigest) {
          payload = p;
          // print('Found existing payload: $payload');
        }
      });

      payload?.snippets.forEach((Snippet s) {
        if (s.digest == payloadTag.snippetDigest) {
          snippet = s;
          // print('Found existing snippet: $snippet');
        }
      });

      snippet?.contentList.forEach((SnippetContent sc) {
        if (sc.digest == payloadTag.snippetContentDigest) {
          snippetContent = sc;
          // print('Found existing snippetContent: $snippetContent');
        }
      });

      int contentVersionIndex = snippetContent == null
          ? 0
          : snippetContent!.contentVersionList.length;

      List<ContentVersion> contentVersionList = snippetContent == null
          ? <ContentVersion>[]
          : snippetContent!.contentVersionList;
      contentVersion = ContentVersion(
          payloadTag.calculateContentVersionDigest(contentVersionIndex),
          contentVersionIndex,
          payloadTag.snippetContentDigest,
          payloadTag.semverPattern);
      contentVersionList.add(contentVersion);

      if (snippetContent == null) {
        List<SnippetContent> snippetContentList =
            snippet == null ? <SnippetContent>[] : snippet!.contentList;
        snippetContent = SnippetContent(
            payloadTag.snippetContentDigest, payloadTag.snippetDigest,
            fileWithPayloads: fileWithPayloads,
            contentVersionList: contentVersionList);
        snippetContentList.add(snippetContent!);

        if (snippet == null) {
          List<Snippet> snippets =
              payload == null ? <Snippet>[] : payload!.snippets;
          snippet = Snippet(payloadTag.snippetDigest, payloadTag.payloadDigest,
              payloadTag.containerName, payloadTag.snippetID,
              contentList: snippetContentList);
          snippets.add(snippet!);

          if (payload == null) {
            payload = Payload(payloadTag.payloadDigest, payloadTag.namespace,
                payloadTag.payloadName,
                snippets: snippets);
            // print("Adding new payload... ${payload!.digest}");
            payloads.add(payload!);
          }
        }
      }
    }
    return ParseResult(
        db, payload!, snippet!, snippetContent!, contentVersion!);
  }

  Future<bool> parsePayloadsFromFile(List<Payload> payloads,
      FileWithPayloads fileWithPayloads, bool replaceContent) async {
    //bool foundPayload = false;
    bool foundBeginning = false;
    ParseResult? parseResult;
    String aggregatedContent = '';
    String? newContentPath;
    String? newContentHash;

    try {
      File fileToParse = File(fileWithPayloads.filepath);
      String filename = pathlib.basename(fileWithPayloads.filepath);
      if (await fileToParse.exists()) {
        log.info('Processing payloads in file: ${fileWithPayloads.filepath}');
        Stream<String> lines = fileToParse
            .openRead()
            .transform(utf8.decoder) // Decode bytes to UTF-8.
            .transform(BaseCommand
                .lineSplitter); // Convert stream to individual lines.

        await for (var line in lines) {
          // print('$line: ${line.length} characters');
          int beginIndex = line.indexOf(PayloadTag.beginTag);
          if (foundBeginning ||
              (beginIndex != -1 &&
                  line.contains(PayloadTag.endOfBeginTag,
                      beginIndex + PayloadTag.beginTag.length))) {
            if (foundBeginning) {
              int endIndex = line.indexOf(PayloadTag.endTag);
              if (endIndex != -1) {
                // foundPayload = true;
                //newPayload.addEnd();
                if (parseResult != null) {
                  //write out the aggregated content to the new content path
                  aggregatedContent = Platform.isWindows
                      ? (aggregatedContent.length > 1
                          ? aggregatedContent.substring(
                              0, aggregatedContent.length - 2)
                          : aggregatedContent)
                      : (aggregatedContent.length > 0
                          ? aggregatedContent.substring(
                              0, aggregatedContent.length - 1)
                          : aggregatedContent);
                  // print('***** the aggregatedContent is: \n$aggregatedContent');
                  newContentHash = getHashOfContent(aggregatedContent);

                  // print('***** the newContentHash is: $newContentHash');
                  newContentPath = pathlib.join(
                      crDirPath,
                      BaseCommand.localContentFolder,
                      parseResult.payload.namespace,
                      parseResult.payload.payloadName,
                      '${parseResult.snippet.containerName}-${parseResult.snippet.snippetID}',
                      'snippet.${parseResult.snippetContent.fileWithPayloads?.digest.toString()}_${parseResult.contentVersion.contentVersionIndex.toString()}.$filename');
                  // print('***** the newContentPath is: $newContentPath');

                  // todo: write out aggregatedContent to filesystem but may need to know if
                  // contentversion already exists in the database before doing this... could overwrite
                  // existing content... especially if content has changed from what is already on the filesystem
                  // another option is just to check if newContentPath already exists on the filesystem
                  // need to have a way to remove all existing payload data for the file currently being processed
                  // and insert the new data because the payload data can be added and removed and changed
                  File newContentFile = File(newContentPath);
                  if (!(await newContentFile.parent.exists())) {
                    await newContentFile.parent.create(recursive: true);
                  }
                  // todo: May need to add logic to compare content hashes to see if they don't match
                  bool newContentFileExists = await newContentFile.exists();
                  if (!newContentFileExists ||
                      (newContentFileExists && replaceContent)) {
                    if (newContentFileExists) {
                      if (newContentHash !=
                          parseResult.dbContentVersion?.contentHash) {
                        print(
                            'Replacing existing payload content in file: $newContentPath');
                        String backupFileName = pathlib.join(
                            pathlib.dirname(newContentPath),
                            '${DateTime.now().millisecondsSinceEpoch}.${pathlib.basename(newContentPath)}.backup');
                        print('Writing backup file: $backupFileName');
                        await newContentFile.copy(backupFileName);
                        await newContentFile.writeAsString(aggregatedContent);
                      } else {
                        log.info(
                            'Content has not changed so not writing to file: $newContentPath');
                      }
                    } else {
                      print(
                          'Writing new payload content to file: $newContentPath');
                      await newContentFile.writeAsString(aggregatedContent);
                    }
                  } else if (newContentHash !=
                      parseResult.dbContentVersion?.contentHash) {
                    print(
                        'Ignoring new payload content in file: ${fileToParse.absolute} -- $newContentHash -- ${parseResult.dbContentVersion?.contentHash}');
                    print(aggregatedContent);
                  } else {
                    log.info(
                        'Content has not changed so not writing to file: $newContentPath');
                  }

                  parseResult.contentVersion.contentPath = newContentPath;
                  parseResult.contentVersion.contentHash = newContentHash;
                  // parseResult.contentVersion.contentSemverVersion = getContentSemverVersion(parseResult.contentVersion.semverPattern);
                  // newPayload.snippets[snippetsIndex].contentList[contentListIndex].contentVersionList[contentVersionIndex].contentSemverVersion = newContentSemverVersion;
                } else {
                  throw PayloadIsNull(
                      "The payload is null in file ${fileWithPayloads.filepath}");
                }
                //newPayload = null;
                aggregatedContent = '';
                newContentPath = null;
                newContentHash = null;
                foundBeginning = false;
                // foundPayload = false;
              } else {
                //newPayload.snippets[snippetsIndex].addLine();
                //newPayload.snippets[snippetsIndex].contentHash = ???;
                // aggregate the content for this payloads snippet
                aggregatedContent +=
                    '$line${(Platform.isWindows ? '\r\n' : '\n')}';
              }
            } else {
              // need to add logic for when the same payload tag is encountered AGAIN in the same
              // file so that the content can be saved off into the BaseCommand.localContentFolder file storage folder

              parseResult = parsePayloadFromBeginTag(
                  payloads,
                  fileWithPayloads,
                  line.substring(
                      beginIndex,
                      line.indexOf(
                              PayloadTag.endOfBeginTag,
                              beginIndex +
                                  PayloadTag.endOfBeginTag.length) +
                          PayloadTag.endOfBeginTag.length));
              foundBeginning = true;
            }
          }
          // else if(foundBeginning && foundPayload) {
          //   if(we found the end tag) {
          //     payloads.add(newPayload);
          //     foundBeginning = false;
          //     foundPayload = false;
          //   }
          // }
        }
      } else {
        log.warning(
            'WARNING: File does not exist: ${fileWithPayloads.filepath}');
      }
    } catch (e, stacktrace) {
      log.severe('[5] Error: $e :: $stacktrace');
    }

    return true;
  }

  @override
  Future<CommandResponse> runCmd() async {
    int returnCode = 0;

    List<String> exclSubStrs =
        (argResults?['exclSubStr'] ?? BaseCommand.excludeSubStrDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();
    List<String> inclSubStrs =
        (argResults?['inclSubStr'] ?? BaseCommand.includeSubStrDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();
    List<String> exclRegExps =
        (argResults?['exclRegExp'] ?? BaseCommand.excludeRegExpDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();
    List<String> inclRegExps =
        (argResults?['inclRegExp'] ?? BaseCommand.includeRegExpDefault)
            .split(',')
            .where((s) => !s.isEmpty)
            .toList();

    // returnCode = await validateBaseDirANDCrDir();

    // if (returnCode == 0) {
    // IMPORTANT NOTE: Only invoke initDB AFTER crDirPath is set to it's final value
    initDB();

    if (argResults?['list'] == true) {
      // print out Payloads already in index
      PayloadList payloadList = PayloadList(db);
      // listOfPayloadFiles.printScannedFiles(includeFullPath, exclSubStrs,
      //     inclSubStrs, exclRegExps, inclRegExps);
      RealmResults<Payload>? payloads = await payloadList.getPayloads();
      print('List of payloads in CodeRockIT database index:');
      payloads?.forEach((payload) {
        // if (includeFullPath(fileWithPayloads.filepath, exclSubStrs,
        //     inclSubStrs, exclRegExps, inclRegExps)) {
        print('$payload');
        // }
      });
    } else {
      // loop over list of scanned files in Index
      // find all of the payload tags in those files and create OR update the payload tags in the Payload
      // index setting important Payload metadata from the crDir configuration
      PayloadFilesList listOfPayloadFiles = PayloadFilesList(db);
      RealmResults<FileWithPayloads>? files =
          await listOfPayloadFiles.getFiles();
      List<Payload> payloads = <Payload>[];
      if (files != null) {
        for (FileWithPayloads fileWithPayloads in files) {
          if (includeFullPath(fileWithPayloads.filepath, exclSubStrs,
              inclSubStrs, exclRegExps, inclRegExps)) {
            await parsePayloadsFromFile(
                payloads, fileWithPayloads, argResults?['replace']);
          }
        }
      }

      if (payloads.isNotEmpty) {
        PayloadList payloadList = PayloadList(db);
        await payloadList.addOrUpdatePayloads(payloads);
      }
    }
    // }

    return CommandResponse(returnCode, verbose, db);
  }
}
