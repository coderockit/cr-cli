import 'dart:io';

import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';
import 'package:cr/database/coderockit_schema.dart';
import 'package:cr/model/payload_list.dart';
import 'package:realm_dart/realm.dart';
import 'package:path/path.dart' as pathlib;
import 'package:pub_semver/pub_semver.dart';

class PushCommand extends BaseCommand {
  @override
  final name = "push";
  @override
  final description =
      "Push to the CodeRockIT API server all of the prepared payloads.";

  PushCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  Future<String> getNewestVersionFromFileSystem(Payload payload) async {
    File newestVerFile = File(pathlib.join(
        crDirPath,
        BaseCommand.remoteContentFolder,
        payload.namespace,
        payload.payloadName,
        'newest-version.txt'));
    if (await newestVerFile.exists()) {
      return newestVerFile.readAsString();
    } else {
      return '0.0.0';
    }
  }

  Future<String> getNewestVersionFromRemoteServer(Payload payload) async {
    return '0.0.0';
  }

  Future<bool> hasLatestContent(Payload? payload) async {
    bool hasLatest = true;
    if (payload != null) {
      // check if the local remotecontent newest version is the same as the newest version on the remote server
      Version fsNewestVersion =
          Version.parse(await getNewestVersionFromFileSystem(payload));
      Version remoteNewestVersion =
          Version.parse(await getNewestVersionFromRemoteServer(payload));
      if (fsNewestVersion < remoteNewestVersion) {
        print(
            'You MUST do a "cr pull" first before pushing payload "${payload.getFullName()}" to the remote server!!!');
        hasLatest = false;
      }
    }
    return hasLatest;
  }

  Future<bool> haveLatestContent(RealmResults<Payload>? payloads) async {
    bool hasLatest = true;
    if (payloads != null) {
      for (var payload in payloads) {
        if (!(await hasLatestContent(payload))) {
          hasLatest = false;
          break;
        }
      }
    }
    return hasLatest;
  }

  @override
  Future<CommandResponse> runCmd() async {
    int returnCode = 0;

    // [run] may also return a Future.
    // print(argResults?['all']);

    // setting the author data to this user

    // IMPORTANT NOTE: Only invoke initDB AFTER crDirPath is set to it's final value
    initDB();

    PayloadList payloadList = PayloadList(db);
    RealmResults<Payload>? payloads = await payloadList.getPayloads();
    print('Preparing payloads to push to remote server...');
    // make sure FIRST that the latest pushed content on the remote server has been pulled before starting the algorithm to push the payloads
    if (await haveLatestContent(payloads)) {
      payloads?.forEach((payload) {
        print('Pushing ${payload.getFullName()} to remote server serverName');

        // - only one ContentVersion tag per Snippet (a Payload consists of many many snippets) can be the main one used to push a new payloadHash
        //   and payloadVersion and so we need an algorithm to decide which one of the many ContentVersion tags per Snippet to select as the main
        //   one to push or how do you allow multiple to be pushed??? such as allowing pushing multiple versions at once!!!! Or the better algorithm
        //   is an on the fly just-in-time prompting the user to select which ContentVersion to use when there is more than one unique contentHash
        //   per snippet (one snippet has a list of SnippetContents and each SnippetContent has a list of ContentVersions)... When the push occurs
        //   it will show the user the current version of the Payload and allow them to select either a major or minor or patch or any-semver defined
        //   change is taking place and then it will prompt them to select the ContentVersion to use when there are multiple that are different ...
        //   sometimes there will not be any changes when compared to what is already pushed to the server and so there will be no need to prompt the user in that case!!!
        // - a payload has a hash and version and the hash is a hash of all of the snippetcontent hashes, each participating snippetcontent does not have a version
        // - you loop over each ContentVersion and decide whether or not the content needs to be pushed to the server and what it's unique version number will be
        // - each unique content hash (Ypn9XxB9Z94PTI3e-ROfVxm8uzQ=) MUST be a unique static version number x.y.z (in the semver style)
        // - if the unique content hash for this payload snippet is not listed with a unique static version number x.y.z (in the semver style) then it has not yet been pushed
        // - if the unique content hash for this payload snippet is not on the server then it has not been pushed and so... push it to the server and get back a unique version number mapped to the unique content hash
        // - if the unique content hash for this payload snippet is already on the server then you do not need to push the content to the server
        // - if the content of the payload snippet is empty (the empty content hash) then do not push it to the server
      });
    }

    return CommandResponse(returnCode, verbose, db);
  }
}
