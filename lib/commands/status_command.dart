import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';

class StatusCommand extends BaseCommand {
  @override
  final name = "status";
  @override
  final description =
      "Check the status of the CodeRockIT payloads in this project.";

  StatusCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);
    return CommandResponse(0, verbose, db);
  }
}
