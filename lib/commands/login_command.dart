import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';

class LoginCommand extends BaseCommand {
  @override
  final name = "login";
  @override
  final description = "Login to the CodeRockIT API server.";

  LoginCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);
    return CommandResponse(0, verbose, db);
  }
}
