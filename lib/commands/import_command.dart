import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/base_command.dart';

class ImportCommand extends BaseCommand {
  @override
  final name = "import";
  @override
  final description =
      "Import the CodeRockIT payloads into the files of this project.";

  ImportCommand() {
    // argParser.addFlag('all', abbr: 'a');
  }

  @override
  Future<CommandResponse> runCmd() async {
    // [run] may also return a Future.
    // print(argResults?['all']);
    return CommandResponse(0, verbose, db);
  }
}
