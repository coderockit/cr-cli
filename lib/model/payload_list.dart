import 'dart:io';
import 'package:cr/database/coderockit_schema.dart';
import 'package:cr/database/realm_db.dart';
import 'package:realm_dart/realm.dart';

class PayloadList {
  final RealmDB? db;

  PayloadList(this.db);

  Future<bool> addOrUpdatePayloads(List<Payload> payloads) async {
    // print('Adding payloads: $payloads');
    db?.realm.write(() {
      for (var payload in payloads) {
        _addOrUpdatePayloadInTx(payload);
      }
    });

    return true;
  }

  bool _addOrUpdatePayloadInTx(Payload payload) {
    // print("Adding or Updating payload ${payload.namespace}/${payload.payloadName} to Payload index!!!");
    Payload? existingPayload = db?.realm.find<Payload>(payload.digest);

    if (existingPayload == null) {
      // print('Adding new payload ${payload.asString()}');
      db?.realm.add(payload);
    } else {
      // print('Modifying existing payload ${existingPayload.asString()}');
      // payload already exists so need to do an update on the existingPayload's snippets
      // reconcile existingPayload.snippets and payload.snippets
      for (var snippet in payload.snippets) {
        Snippet? existingSnippet = db?.realm.find<Snippet>(snippet.digest);
        if (existingSnippet == null) {
          // print('Adding new snippet ${snippet.asString()}');
          db?.realm.add(snippet);
          if (!existingPayload.snippets.contains(snippet)) {
            existingPayload.snippets.add(snippet);
          }
        } else {
          // print('Modifying existing snippet ${existingSnippet.asString()}');
          // snippet already exists so need to do an update on the existingSnippet's contentList
          // reconcile existingSnippet.contentList and snippet.contentList
          if (!existingPayload.snippets.contains(existingSnippet)) {
            existingPayload.snippets.add(existingSnippet);
          }
          for (var snippetContent in snippet.contentList) {
            SnippetContent? existingSnippetContent =
                db?.realm.find<SnippetContent>(snippetContent.digest);
            if (existingSnippetContent == null) {
              // print('Adding new snippetContent ${snippetContent.asString()}');
              db?.realm.add(snippetContent);
              if (!existingSnippet.contentList.contains(snippetContent)) {
                existingSnippet.contentList.add(snippetContent);
              }
            } else {
              // print('Modifying existing snippetContent ${existingSnippetContent.asString()}');
              // snippetContent already exists so need to do an update on the existingSnippetContent's contentVersionList
              // reconcile existingSnippetContent.contentVersionList and snippetContent.contentVersionList
              if (!existingSnippet.contentList
                  .contains(existingSnippetContent)) {
                existingSnippet.contentList.add(existingSnippetContent);
              }

              int maxloop = snippetContent.contentVersionList.length;
              if (existingSnippetContent.contentVersionList.length >
                  snippetContent.contentVersionList.length) {
                maxloop = existingSnippetContent.contentVersionList.length;
              }
              //else {

              for (int i = 0; i < maxloop; ++i) {
                if (i < existingSnippetContent.contentVersionList.length &&
                    i < snippetContent.contentVersionList.length) {
                  ContentVersion? existingContentVersion =
                      existingSnippetContent.contentVersionList.elementAt(i);
                  ContentVersion? newContentVersion =
                      existingSnippetContent.contentVersionList.elementAt(i);
                  existingContentVersion.semverPattern =
                      newContentVersion.semverPattern;
                  existingContentVersion.contentPath =
                      newContentVersion.contentPath;
                  existingContentVersion.contentHash =
                      newContentVersion.contentHash;
                  // existingContentVersion.contentSemverVersion =
                  //     newContentVersion.contentSemverVersion;
                  // print('Modified existing contentVersionList -- ${existingSnippetContent.contentVersionList}');
                } else if (i <
                        existingSnippetContent.contentVersionList.length &&
                    i >= snippetContent.contentVersionList.length) {
                  // print('Deleting part of existing contentVersionList [$i, ${existingSnippetContent.contentVersionList.length}] -- ${existingSnippetContent.contentVersionList}');
                  Iterable<ContentVersion> toDelete =
                      existingSnippetContent.contentVersionList.getRange(
                          i, existingSnippetContent.contentVersionList.length);
                  existingSnippetContent.contentVersionList.removeRange(
                      i, existingSnippetContent.contentVersionList.length);
                  for (var delContentVersion in toDelete) {
                    File(delContentVersion.contentPath!).deleteSync();
                    db?.realm.delete(delContentVersion);
                  }
                  break;
                } else if (i >=
                        existingSnippetContent.contentVersionList.length &&
                    i < snippetContent.contentVersionList.length) {
                  // print('Adding new content to existing contentVersionList [$i, ${snippetContent.contentVersionList.length}] -- ${snippetContent.contentVersionList}');
                  existingSnippetContent.contentVersionList.addAll(
                      snippetContent.contentVersionList.getRange(
                          i, snippetContent.contentVersionList.length));
                  break;
                }
              }

              // for (var contentVersion in snippetContent.contentVersionList) {
              //   ContentVersion? existingContentVersion = db?.realm.find<ContentVersion>(contentVersion.digest);
              //   if(existingContentVersion == null) {
              //     db?.realm.add(contentVersion);
              //     if(!existingSnippetContent.contentVersionList.contains(contentVersion)) {
              //       existingSnippetContent.contentVersionList.add(contentVersion);
              //     }
              //   } else {
              //     // contentVersion already exists so verify its in contentVersionList and update its data
              //     if(!existingSnippetContent.contentVersionList.contains(existingContentVersion)) {
              //       existingSnippetContent.contentVersionList.add(existingContentVersion);
              //     }

              //     existingContentVersion.semverPattern = contentVersion.semverPattern;
              //     existingContentVersion.contentPath = contentVersion.contentPath;
              //     existingContentVersion.contentHash = contentVersion.contentHash;
              //     existingContentVersion.contentSemverVersion = contentVersion.contentSemverVersion;
              //   }
              // }
            }
          }
        }
      }
    }

    return true;
  }

  Future<RealmResults<Payload>?> getPayloads() async {
    RealmResults<Payload>? payloads = db?.realm.all<Payload>();
    return payloads;
  }
}

class AbortSearchingInFile implements Exception {
  final String message;
  const AbortSearchingInFile([this.message = ""]);
  @override
  String toString() {
    return '$runtimeType: $message';
  }
}

class PayloadIsNull implements Exception {
  final String message;
  const PayloadIsNull([this.message = ""]);
  @override
  String toString() {
    return '$runtimeType: $message';
  }
}

// class IncorrectPayloadContainerName implements Exception {
//   final String message;
//   const IncorrectPayloadContainerName([this.message = ""]);

//   @override
//   String toString() {
//     return '$runtimeType: $message';
//   }
// }
