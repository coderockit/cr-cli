import 'package:cr/database/coderockit_schema.dart';
import 'package:cr/database/realm_db.dart';

class ParseResult {
  final RealmDB? db;
  final Payload payload;
  final Snippet snippet;
  final SnippetContent snippetContent;
  final ContentVersion contentVersion;

  late Payload? dbPayload;
  late Snippet? dbSnippet;
  late SnippetContent? dbSnippetContent;
  late ContentVersion? dbContentVersion;

  ParseResult(this.db, this.payload, this.snippet, this.snippetContent,
      this.contentVersion) {
    dbPayload = db?.realm.find<Payload>(payload.digest);
    dbSnippet = db?.realm.find<Snippet>(snippet.digest);
    dbSnippetContent = db?.realm.find<SnippetContent>(snippetContent.digest);
    dbContentVersion = db?.realm.find<ContentVersion>(contentVersion.digest);
  }
}
