class PayloadFile {
  final int digest;
  final bool exists;
  final String filePath;

  PayloadFile(this.digest, this.exists, this.filePath);
}
