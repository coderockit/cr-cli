import 'dart:convert';
import 'dart:typed_data';
import 'package:xxh3/xxh3.dart';

class PayloadTag {
  static final String beginTag = "-+=coderockit:/@";
  static final String beginOfBeginTag = "-+=";
  static final String endOfBeginTag = "]==";
  static final String endTag = "-+=coderockit:/@]==";

  final String originalTag;
  final String filepath;
  bool parsed = false;

  late final String namespace;
  late final String payloadName;
  late final String semverPattern;
  late final String containerName;
  late final int snippetID;
  late final int payloadDigest;
  late final int snippetDigest;
  late final int snippetContentDigest;

  PayloadTag(this.originalTag, this.filepath) {
    int beginIndex = originalTag.indexOf(beginTag);
    if (beginIndex != -1) {
      int nameIndex = beginIndex + beginTag.length - 1;
      int endNameIndex = originalTag.indexOf('@', nameIndex + 1);
      List<String> nameParts =
          originalTag.substring(nameIndex, endNameIndex).split('/');
      namespace = nameParts[0];
      payloadName = nameParts[1];
      semverPattern = originalTag.substring(
          endNameIndex + 1, originalTag.indexOf('/', endNameIndex));
      List<String> containerNameParts = originalTag
          .substring(originalTag.lastIndexOf('/') + 1,
              originalTag.lastIndexOf(endOfBeginTag))
          .split('-');
      containerName = containerNameParts[0];
      // if (!filepath.endsWith(containerName)) {
      //   // throw IncorrectPayloadContainerName(
      //   //     'The container name $containerName does not match ${pathlib.basename(filepath)}');
      // }
      snippetID = int.parse(containerNameParts[1]);

      payloadDigest = xxh3(Uint8List.fromList(utf8.encode(getFullName())));

      Uint8List containerNameBytes =
          Uint8List.fromList(utf8.encode('${getFullName()}/$containerName'));
      // Uint8List intBytes = Uint8List(8);
      // intBytes.buffer.asUint32List(0, 1)[0] = payloadDigest;
      // intBytes.buffer.asUint32List(4, 1)[0] = snippetID;
      Uint8List snippetIDBytes = Uint8List(4);
      snippetIDBytes.buffer.asUint32List(0, 1)[0] = snippetID;
      snippetDigest = xxh3(Uint8List.fromList(
          containerNameBytes.toList(growable: false) +
              snippetIDBytes.toList(growable: false)));

      Uint8List snippetContentDigestBytes = Uint8List.fromList(
          utf8.encode('${getFullName()}/$containerName$filepath'));
      //intBytes.buffer.asUint32List(0, 1)[0] = snippetDigest;
      //intBytes.buffer.asUint32List(4, 1)[0] = fileDigest;
      snippetContentDigest = xxh3(Uint8List.fromList(
          snippetContentDigestBytes.toList(growable: false) +
              snippetIDBytes.toList(growable: false)));

      parsed = true;
    }
  }

  String getFullName() {
    return '$namespace/$payloadName';
  }

  bool get isParsed {
    return parsed;
  }

  int calculateContentVersionDigest(int contentVersionIndex) {
    Uint8List contentVersionDigestBytes = Uint8List(8);
    contentVersionDigestBytes.buffer.asUint32List(0, 1)[0] =
        contentVersionIndex;
    contentVersionDigestBytes.buffer.asUint32List(4, 1)[0] =
        snippetContentDigest;
    return xxh3(contentVersionDigestBytes);
  }
}
