import 'dart:io';

import 'package:cr/database/coderockit_schema.dart';
import 'package:cr/database/realm_db.dart';
import 'package:realm_dart/realm.dart';

class PayloadFilesList {
  final RealmDB? db;

  PayloadFilesList(this.db);

  Future<bool> addFiles(List<FileWithPayloads> filesWithPayloads) async {
    db?.realm.write(() {
      for (var payloadFile in filesWithPayloads) {
        _addFileInTx(payloadFile);
      }
    });

    return true;
  }

  bool _addFileInTx(FileWithPayloads payloadFile) {
    FileWithPayloads? existingFile =
        db?.realm.find<FileWithPayloads>(payloadFile.digest);
    if (existingFile == null) {
      print("Adding file ${payloadFile.filepath} to FileWithPayloads index!!!");
      db?.realm.add(payloadFile);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> addFile(FileWithPayloads fileWithPayloads) async {
    bool fileAdded = false;
    db?.realm.write(() {
      fileAdded = _addFileInTx(fileWithPayloads);
    });
    return fileAdded;
  }

  Future<RealmResults<FileWithPayloads>?> getFiles() async {
    RealmResults<FileWithPayloads>? files = db?.realm.all<FileWithPayloads>();
    return files;
  }

  Future<bool> cleanupFiles(List<FileWithPayloads> filesToRemove) async {
    db?.realm.write(() {
      bool retVal = removeAllPayloadsForFiles(filesToRemove, startTx: false);
      if (retVal == true) {
        db?.realm.deleteMany(filesToRemove);
      }
    });
    return true;
  }

  bool removeAllPayloadsForFiles(List<FileWithPayloads> filesToRemove,
      {bool startTx = true}) {
    if (startTx) {
      db?.realm.write(() {
        for (var payloadFile in filesToRemove) {
          removeAllPayloadsForFile(payloadFile, startTx: false);
        }
      });
    } else {
      for (var payloadFile in filesToRemove) {
        removeAllPayloadsForFile(payloadFile, startTx: startTx);
      }
    }
    return true;
  }

  bool removeAllPayloadsForFile(FileWithPayloads fileWithPayloads,
      {bool startTx = false}) {
    if (startTx) {
      db?.realm.write(() {
        _removePayloadsForFileInTx(fileWithPayloads);
      });
    } else {
      _removePayloadsForFileInTx(fileWithPayloads);
    }
    return true;
  }

  bool _removePayloadsForFileInTx(FileWithPayloads fileWithPayloads) {
    var contentList = db?.realm
        .all<SnippetContent>()
        .query('fileWithPayloads.digest == ${fileWithPayloads.digest}');

    if (contentList != null) {
      // print('found SnippetContents to delete: $contentList');
      //delete all of the ContentVersion objects in the SnippetContent objects in contentList
      Map<int, int> removedSnippetContentCounters = {};
      Map<int, int> removedSnippetCounters = {};
      for (var snippetContent in contentList) {
        for (var contentVersion in snippetContent.contentVersionList) {
          if (contentVersion.contentPath != null) {
            File contentFile = File(contentVersion.contentPath!);
            if (contentFile.existsSync()) {
              print("Deleted file: ${contentFile.absolute}");
              contentFile.deleteSync();
            }
          }
          // DO NOT delete contentVersion here as it causes a concurrent modification
          // exception for the snippetContent.contentVersionList array
          // db?.realm.delete(contentVersion);
        }
        print("Deleted content versions: ${snippetContent.contentVersionList}");
        db?.realm.deleteMany(snippetContent.contentVersionList);

        //check if need to delete snippet
        Snippet? snippet =
            db?.realm.find<Snippet>(snippetContent.snippetDigest);
        if (snippet != null) {
          int? removedSnippetContentCount =
              removedSnippetContentCounters[snippetContent.snippetDigest];
          if (removedSnippetContentCount == null) {
            removedSnippetContentCount = 1;
            removedSnippetContentCounters[snippetContent.snippetDigest] =
                removedSnippetContentCount;
          }
          // print(
          //     '${snippetContent.snippetDigest} :: snippet.contentList.length :: ${snippet.contentList.length}');
          // print(
          //     '${snippetContent.snippetDigest} :: removedSnippetContentCount :: $removedSnippetContentCount');
          if (snippet.contentList.length - removedSnippetContentCount == 0) {
            Payload? payload = db?.realm.find<Payload>(snippet.payloadDigest);
            if (payload != null) {
              int? removedSnippetCount =
                  removedSnippetCounters[snippet.payloadDigest];
              if (removedSnippetCount == null) {
                removedSnippetCount = 1;
                removedSnippetCounters[snippet.payloadDigest] =
                    removedSnippetCount;
              }
              if (payload.snippets.length - removedSnippetCount == 0) {
                print('Deleted payload: $payload');
                db?.realm.delete(payload);
              } else {
                // NOTE: Do NOT remove from list as the delete already removes it from the list
                //payload.snippets.remove(snippet);
                removedSnippetCounters[snippet.payloadDigest] =
                    removedSnippetCount + 1;
              }
            }
            print('Deleted snippet: $snippet');
            db?.realm.delete(snippet);
          } else {
            // NOTE: Do NOT remove from list as the delete already removes it from the list
            //snippet.contentList.remove(snippetContent);
            removedSnippetContentCounters[snippetContent.snippetDigest] =
                removedSnippetContentCount + 1;
          }
        }
        print('Deleted snippet content: $snippetContent');
        db?.realm.delete(snippetContent);
      }
    }
    return true;
  }
}
