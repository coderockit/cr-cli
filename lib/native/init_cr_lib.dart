import 'dart:ffi';
import 'dart:io';
import 'package:dylib/dylib.dart';

// import 'cli/metrics/metrics_command.dart';
// import 'cli/metrics/options.dart';
// import 'cli/common/target_os_type.dart';

// import '../coderockit.dart' as coderockit show isFlutterPlatform;
// import '../coderockit.dart' show coderockitBinaryName;

const String coderockitBinaryName = 'coderockit';

DynamicLibrary? _library;

void _debugWrite(String message) {
  assert(() {
    print(message);
    return true;
  }());
}

String _resolveLibPath(String binaryName, String binaryPath) {
  String libPath = resolveDylibPath(
    binaryName, // foo.dll, libfoo.so, libfoo.dylib...
    dartDefine: 'LIB${binaryName.toUpperCase()}_PATH',
    environmentVariable: 'LIB${binaryName.toUpperCase()}_PATH',
  );
  if (FileSystemEntityType.notFound == FileSystemEntity.typeSync(libPath)) {
    libPath = resolveDylibPath(
      binaryName, // foo.dll, libfoo.so, libfoo.dylib...
      path: binaryPath,
      dartDefine: 'LIB${binaryName.toUpperCase()}_PATH',
      environmentVariable: 'LIB${binaryName.toUpperCase()}_PATH',
    );
  }
  return libPath;
}

String _getBinaryPath(String binaryName) {
  if (Platform.isAndroid) {
    return "lib$binaryName.so";
  } else if (Platform.isLinux) {
    if (isFlutterPlatform) {
      return '${File(Platform.resolvedExecutable).parent.path}/lib/lib$binaryName.so';
    }

    return _resolveLibPath(binaryName, 'binary/linux');
  } else if (Platform.isMacOS) {
    if (isFlutterPlatform) {
      return "${File(Platform.resolvedExecutable).parent.absolute.path}/../Frameworks/coderockit.framework/Resources/lib$binaryName.dylib";
    }

    return _resolveLibPath(
        binaryName, '${Directory.current.path}/binary/macos');
  } else if (Platform.isIOS) {
    return "${File(Platform.resolvedExecutable).parent.absolute.path}/Frameworks/coderockit_dart.framework/coderockit_dart";
  } else if (Platform.isWindows) {
    if (isFlutterPlatform) {
      return "$binaryName.dll";
    }

    return _resolveLibPath(binaryName, 'binary/windows');
  }

  throw UnsupportedError(
      "Platform ${Platform.operatingSystem} is not supported");
}

bool get isFlutterPlatform => false; // coderockit.isFlutterPlatform;

/// @nodoc
// Initializes coderockit library
DynamicLibrary initCodeRockIT() {
  if (_library != null) {
    return _library!;
  }

  if (!isFlutterPlatform) {
    assert(() {
      // try {
      //   uploadMetrics(Options(
      //     targetOsType: Platform.operatingSystem.asTargetOsType,
      //     targetOsVersion: Platform.operatingSystemVersion,
      //   ));
      // } catch (_) {} // ignore: avoid_catching_errors
      return true;
    }());
  }

  final coderockitBinaryPath = _getBinaryPath(coderockitBinaryName);
  final coderockitLibrary = DynamicLibrary.open(coderockitBinaryPath);

  // final initializeApi = coderockitLibrary.lookupFunction<
  //     IntPtr Function(Pointer<Void>),
  //     int Function(Pointer<Void>)>("coderockit_dart_initializeDartApiDL");
  // var initResult = initializeApi(NativeApi.initializeApiDLData);
  // if (initResult != 0) {
  //   throw AssertionError(
  //       "coderockit initialization failed. Error: could not initialize Dart APIs");
  // }

  return _library = coderockitLibrary;
}
