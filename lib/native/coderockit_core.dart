import 'dart:convert';
import 'dart:io';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:cr/model/payload_file.dart';
import 'package:cr/native/coderockit_bindings.dart';
import 'package:cr/native/init_cr_lib.dart';
// Hide StringUtf8Pointer.toNativeUtf8 and StringUtf16Pointer since these allows silently allocating memory. Use toUtf8Ptr instead
import 'package:ffi/ffi.dart' hide StringUtf8Pointer, StringUtf16Pointer;
import 'package:path/path.dart' as pathlib;

late CodeRockITLibrary _crLib;

final _CodeRockITCore crCore = _CodeRockITCore();

const int MIN_INT64 = -9223372036854775808;

enum LogLevel {
  // ignore: constant_identifier_names
  OFF, // 0
  // ignore: constant_identifier_names
  ERROR, // 1
  // ignore: constant_identifier_names
  WARN, // 2
  // ignore: constant_identifier_names
  INFO, // 3
  // ignore: constant_identifier_names
  DEBUG, // 4
  // ignore: constant_identifier_names
  TRACE, // 5
}

class _CodeRockITCore {
  static _CodeRockITCore? _instance;

  _CodeRockITCore._() {
    final lib = initCodeRockIT();
    _crLib = CodeRockITLibrary(lib);
  }

  factory _CodeRockITCore() {
    return _instance ??= _CodeRockITCore._();
  }

  // LastError? getLastError(Allocator allocator) {
  //   final error = allocator<realm_error_t>();
  //   final success = _crLib.realm_get_last_error(error);
  //   if (!success) {
  //     return null;
  //   }

  //   final message = error.ref.message.cast<Utf8>().toRealmDartString();

  //   return LastError(error.ref.error, message);
  // }

  // void throwLastError([String? errorMessage]) {
  //   using((Arena arena) {
  //     final lastError = getLastError(arena);
  //     throw CodeRockITNativeException('${errorMessage != null ? errorMessage + ". " : ""}${lastError ?? ""}');
  //   });
  // }

  int add(int x, int y) {
    return _crLib.add(x, y);
  }

  // int doubleIt(int x) {
  //   return _crLib.DoubleIt(x);
  // }

  int scanForFilesWithPayloads(
      Directory baseDir,
      Directory crDir,
      LogLevel logLevel,
      List<String> exclSubStrs,
      List<String> inclSubStrs,
      List<String> exclRegExps,
      List<String> inclRegExps,
      {List<String>? folders}) {
    return using<int>((Arena arena) {
      Pointer<Char> goBaseDir = baseDir.absolute.path.toCharPtr(arena);
      Pointer<Char> goCrDir = crDir.absolute.path.toCharPtr(arena);
      Pointer<Pointer<Char>> goExclSubStrs = exclSubStrs.toCharPtrPtr(arena);
      Pointer<Pointer<Char>> goInclSubStrs = inclSubStrs.toCharPtrPtr(arena);
      Pointer<Pointer<Char>> goExclRegExps = exclRegExps.toCharPtrPtr(arena);
      Pointer<Pointer<Char>> goInclRegExps = inclRegExps.toCharPtrPtr(arena);
      Pointer<Pointer<Char>> logPtr = <String>[].toCharPtrPtr(arena);
      Pointer<Pointer<Char>> errKindPtr = <String>[].toCharPtrPtr(arena);
      Pointer<Pointer<Char>> errMsgPtr = <String>[].toCharPtrPtr(arena);

      if (folders == null) {
        Pointer<Pointer<Char>> goFolders = <String>[].toCharPtrPtr(arena);
        final returnCode = _crLib.scanForFilesWithPayloads(
            goBaseDir,
            goCrDir,
            logLevel.index,
            goExclSubStrs,
            goInclSubStrs,
            goExclRegExps,
            goInclRegExps,
            goFolders,
            logPtr,
            errKindPtr,
            errMsgPtr);
        final logStr = logPtr.value.cast<Utf8>();
        print(
            "[1] _crLib.scanForFilesWithPayloads returnCode is: $returnCode with $logStr -- and with ${logStr.toDartString()}");
        // print(
        //     "[2] _crLib.scanForFilesWithPayloads with logPtr.value.elementAt(500).value: ${logPtr.value.elementAt(500).value}");
        _crLib.coderockit_cstring_free(logPtr.value);
        _crLib.coderockit_cstring_free(errKindPtr.value);
        _crLib.coderockit_cstring_free(errMsgPtr.value);
        return returnCode;
      } else {
        List<String> canonicalFolders = <String>[];
        for (String folder in folders) {
          Directory folderDir = Directory(pathlib.canonicalize(folder));
          if (folderDir.existsSync()) {
            canonicalFolders.add(folderDir.absolute.path);
          } else {
            print(
                '[6] ERROR: The folder "${folderDir.path}" does not exist, cannot scan this folder!!!');
          }
        }
        Pointer<Pointer<Char>> goFolders = canonicalFolders.toCharPtrPtr(arena);
        final returnCode = _crLib.scanForFilesWithPayloads(
            goBaseDir,
            goCrDir,
            logLevel.index,
            goExclSubStrs,
            goInclSubStrs,
            goExclRegExps,
            goInclRegExps,
            goFolders,
            logPtr,
            errKindPtr,
            errMsgPtr);
        final logStr = logPtr.value.cast<Utf8>();
        print(
            "[3] _crLib.scanForFilesWithPayloads returnCode is: $returnCode with $logStr -- and with ${logStr.toDartString()}");
        // print(
        //     "[4] _crLib.scanForFilesWithPayloads with logPtr.value.elementAt(500).value: ${logPtr.value.elementAt(500).value}");
        _crLib.coderockit_cstring_free(logPtr.value);
        _crLib.coderockit_cstring_free(errKindPtr.value);
        _crLib.coderockit_cstring_free(errMsgPtr.value);
        return returnCode;
      }
    });
  }

  // List<PayloadFile> getScannedFilesList(
  //     Directory baseDir,
  //     Directory crDir,
  //     LogLevel logLevel,
  //     List<String> exclSubStrs,
  //     List<String> inclSubStrs,
  //     List<String> exclRegExps,
  //     List<String> inclRegExps,
  //     int lastPayloadFileDigest) {
  //   return using<List<PayloadFile>>((Arena arena) {
  //     GoString goBaseDir = baseDir.absolute.path.toGoString(arena).ref;
  //     GoString goCrDir = crDir.absolute.path.toGoString(arena).ref;
  //     GoSlice goExclSubStrs = exclSubStrs.toGoSlice(arena).ref;
  //     GoSlice goInclSubStrs = inclSubStrs.toGoSlice(arena).ref;
  //     GoSlice goExclRegExps = exclRegExps.toGoSlice(arena).ref;
  //     GoSlice goInclRegExps = inclRegExps.toGoSlice(arena).ref;
  //     List<PayloadFile> list = [];
  //     int scannedFiles = _crLib.GetScannedFilesList(
  //         goBaseDir,
  //         goCrDir,
  //         logLevel.index,
  //         goExclSubStrs,
  //         goInclSubStrs,
  //         goExclRegExps,
  //         goInclRegExps,
  //         lastPayloadFileDigest);
  //     // print('first returned value handle: $scannedFiles');
  //     final file = arena<payloadFile>();
  //     int foundOne;
  //     while ((foundOne = _crLib.GetNextPayloadFile(scannedFiles, file)) == 0) {
  //       StringBuffer buf = StringBuffer();
  //       for (int i = 0; i < file.ref.filePathSize; ++i) {
  //         buf.writeCharCode(file.ref.filePath[i]);
  //       }
  //       // print(
  //       //     'The first file path is: $buf with digest: ${file.ref.Digest} and if it exists: ${file.ref.Exists}');
  //       list.add(PayloadFile(file.ref.digest, file.ref.exists, buf.toString()));
  //     }
  //     return list;
  //   });
  // }
}

// class CodeRockITNativeException implements Exception {
//   final String message;

//   CodeRockITNativeException(this.message);

//   @override
//   String toString() {
//     return "CodeRockITNativeException: $message";
//   }
// }

// class LastError {
//   final int code;
//   final String? message;

//   LastError(this.code, [this.message]);

//   @override
//   String toString() {
//     return "Error code: $code ${(message != null ? ". Message: $message" : "")}";
//   }
// }

// extension on List<String> {
//   Pointer<GoString> toGoStringPtr(Allocator allocator) {
//     final result = allocator<GoString>(length);
//     for (int i = 0; i < length; ++i) {
//       final nativeStr = elementAt(i).toGoString(allocator);
//       final goString = result.elementAt(i).ref;
//       goString.p = nativeStr.ref.p;
//       goString.n = nativeStr.ref.n;
//     }
//     return result.cast();
//   }

//   Pointer<GoSlice> toGoSlice(Allocator allocator) {
//     final goSlice = allocator<GoSlice>();
//     goSlice.ref.data = toGoStringPtr(allocator).cast(); // nullprt;
//     goSlice.ref.len = length;
//     goSlice.ref.cap = length;
//     return goSlice;
//   }
// }

extension on List<String> {
  Pointer<Pointer<Char>> toCharPtrPtr(Allocator allocator) {
    final result = allocator<Pointer<Char>>(length);
    for (int i = 0; i < length; ++i) {
      final nativeStr = elementAt(i).toCharPtr(allocator);
      result.elementAt(i).value = nativeStr;
    }
    return result;
  }

  // Pointer<GoSlice> toGoSlice(Allocator allocator) {
  //   final goSlice = allocator<GoSlice>();
  //   goSlice.ref.data = toGoStringPtr(allocator).cast(); // nullprt;
  //   goSlice.ref.len = length;
  //   goSlice.ref.cap = length;
  //   return goSlice;
  // }
}

extension on List<int> {
  Pointer<Char> toCharPtr(Allocator allocator) {
    return toUint8Ptr(allocator).cast();
  }

  Pointer<Uint8> toUint8Ptr(Allocator allocator) {
    final nativeSize = length;
    final result = allocator<Uint8>(nativeSize);
    final Uint8List native = result.asTypedList(nativeSize);
    native.setAll(0, this); // copy
    return result.cast();
  }
}

extension _StringEx on String {
  Pointer<Char> toCharPtr(Allocator allocator) {
    final units = utf8.encode(this);
    return units.toCharPtr(allocator).cast();
  }

  // Pointer<Pointer<Char>> toGoString(Allocator allocator) {
  //   final goString = allocator<Pointer<Char>>();
  //   goString.value = toCharPtr(allocator);
  //   //final units = utf8.encode(this);
  //   //goString.ref.n = units.length;
  //   return goString;
  // }
}

// extension _CodeRockITLibraryEx on CodeRockITLibrary {
//   void invokeGetBool(bool Function() callback, [String? errorMessage]) {
//     bool success = callback();
//     if (!success) {
//       // crCore.throwLastError(errorMessage);
//       throw CodeRockITNativeException('Invoking callback function returned false!!');
//     }
//   }

//   Pointer<T> invokeGetPointer<T extends NativeType>(Pointer<T> Function() callback, [String? errorMessage]) {
//     final result = callback();
//     if (result == nullptr) {
//       // crCore.throwLastError(errorMessage);
//       throw CodeRockITNativeException('Invoking callback function returned null!!');
//     }
//     return result;
//   }
// }
