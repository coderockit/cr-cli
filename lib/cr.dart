import 'package:args/command_runner.dart';
import 'package:cr/commands/base_command.dart';
import 'package:cr/commands/pull_command.dart';
import 'package:cr/commands/payload_command.dart';
import 'package:cr/commands/command_response.dart';
import 'package:cr/commands/diff_command.dart';
import 'package:cr/commands/scanfs_command.dart';
import 'package:cr/commands/import_command.dart';
import 'package:cr/commands/login_command.dart';
import 'package:cr/commands/plugin_command.dart';
import 'package:cr/commands/push_command.dart';
import 'package:cr/commands/status_command.dart';
import 'package:cr/commands/version_command.dart';
import 'package:cr/native/coderockit_core.dart';
import 'package:cr/tools/logger.dart';

Future<int> run(List<String> arguments) async {
  print(
      'runtimeType is: ${crCore.runtimeType} and add is: ${crCore.add(330, 67)}');

  CommandRunner cmdRun = CommandRunner("cr",
      "CodeRockIT: Code is the Library -- command line tool to manage CodeRockIT payloads")
    ..addCommand(ScanfsCommand())
    ..addCommand(LoginCommand())
    ..addCommand(PayloadCommand())
    ..addCommand(PushCommand())
    ..addCommand(PullCommand())
    ..addCommand(StatusCommand())
    ..addCommand(ImportCommand())
    ..addCommand(PluginCommand())
    ..addCommand(VersionCommand())
    ..addCommand(DiffCommand())
    ..argParser.addOption('verbose',
        abbr: 'v',
        help:
            'Turn on verbose mode using integers 0 = ERROR, 1 = WARN, 2 = INFO, or 3 = DEBUG.',
        defaultsTo: LogLevel.ERROR.toString())
    ..argParser.addOption('baseDir',
        abbr: 'b',
        help:
            'The directory of where to start searching for files containing CodeRockIT payloads. Defaults to "${BaseCommand.baseDirDefault}"')
    ..argParser.addOption('crDir',
        abbr: 'c',
        help:
            'The directory of where to search for project specific config, authoring metadata, and a project specific payload index database. Defaults to "${BaseCommand.crDirDefault}"');

  CommandResponse? response = await cmdRun.run(arguments);
  if (response != null && response.exitCode != 0) {
    log.warning(
        'The response from run with arguments $arguments is: ${response.exitCode}');
  }

  response?.db?.close();

  return response?.exitCode ?? 0;
}

// - [ ] cr login - generates a url to put in a browser where you login and then enter the token back in the cr login prompt OR it should do what the firebase command-line login does…
// - [ ] cr payload - takes the coderockit payloads and prepares the payloads to be published to the coderocket server or imported into your local code… also looks for meta-data about the coderockit payloads like programming language, private/public, anonymous/authenticated, author name, etc., list of snippetNames mapped to fullpath filenames
// - [ ] cr publish - sends the authored payloads to the remote server
// - [ ] cr status - show what status of payloads is to be able to see what would be sent/published to the server
// - [ ] cr import - pulls the code from the coderockit server and imports it into the files
// - [ ] cr scanfs - looks for coderockit payloads in files and stores the fullpath file locations in a sql-lite database or the same database tech stack that git uses
// - [ ] cr plugin - command for managing/adding/removing/configuring plugins
// - [ ] cr version - command for outputting the version of the cr cli
// - [ ] cr diff - show diffs with found payloads when compared to the published payloads
