#!/bin/sh
export PUB_VER=`grep "version:" ./pubspec.yaml|awk '{print $2}'`
export LIBREALM_DART_PATH="$PWD/binary/macos"
BUILD_NUM=`cat ../cr-lib-rust/buildnum`
BUILD_NUM=$((BUILD_NUM-1))
export LIBCODEROCKIT_PATH="$PWD/binary/macos/coderockit/$BUILD_NUM"
if [ -f "../cr-lib-rust/target$BUILD_NUM/libcoderockit.h" ] && [ ! -f "binary/macos/coderockit/$BUILD_NUM/libcoderockit.dylib" ]
then
    echo "Generating FFI for libcoderockit"
    rm -rf binary/macos/coderockit
    mkdir -p binary/macos/coderockit/$BUILD_NUM
    cp ../cr-lib-rust/target$BUILD_NUM/libcoderockit.h ffigen
    cp ../cr-lib-rust/target$BUILD_NUM/aarch64-apple-darwin/release/libcoderockit.dylib binary/macos/coderockit/$BUILD_NUM
    cd ffigen
    dart pub get
    dart run ffigen --config config.yaml
    cd ..
fi
mkdir -p build/$PUB_VER/$OSTYPE
dart pub get
dart run realm_dart install
dart run realm_dart generate
dart format --set-exit-if-changed bin/ lib/ test/
dart analyze .
# dart run bin/main.dart scanfs -v --inclSubStr cr-cli/lib
# dart test --platform vm --timeout 30s --concurrency=6 --reporter=expanded --coverage=coverage
dart run coverage:test_with_coverage
# Install genhtml command with: brew install lcov
genhtml coverage/lcov.info -o coverage/html
dart run dlcov -c 80
dart compile exe bin/main.dart -o build/$PUB_VER/$OSTYPE/cr
security find-identity -p codesigning
codesign -s E28C369219F7063CD87EAFA81DADB86B58C25814 build/$PUB_VER/$OSTYPE/cr
codesign -dv --verbose=4 build/$PUB_VER/$OSTYPE/cr
# sudo cp build/$PUB_VER/$OSTYPE/cr /usr/local/bin
