#!/bin/sh

rm -rf build
rm -rf coverage
rm -rf binary
# sudo rm /usr/local/bin/cr
# sudo rm -f /usr/local/lib/librealm_dart.dylib
rm -rf .coderockit/coderockit.realm
rm -rf .coderockit/coderockit.realm.*
rm -rf .coderockit/localcontent
rm -rf test/.coderockit/coderockit.realm
rm -rf test/.coderockit/coderockit.realm.*
rm -rf test/.coderockit/localcontent
rm -rf ~/.coderockit/coderockit.realm
rm -rf ~/.coderockit/coderockit.realm.*
rm -rf ~/.coderockit/localcontent
